var LocalStrategy = require('passport-local').Strategy;

var UserModel = require('../model/User');

module.exports = function(passport){
	passport.serializeUser(function(user, done){
		done(null, user.id);
	});
	passport.deserializeUser(function(id, done){
		UserModel.findById(id, function(err, user){
			done(err, user);
		});
	});
	passport.use('local-login',new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true,
	},
	function(req, username, password, done){
		UserModel.findOne({'username': username}, function(err, user){
			req.flash('loginMessage', '');
			if (err) { return done(err); }
			if (!user) { 
				return done(null, false, req.flash('loginMessage', 'Username is invalid')); 
			}
			if (!user.validPassword(password)) { 
				return done(null, false, req.flash('loginMessage', 'Password is invalid.')); 
			}
			return done(null, user);
		});
	}));
};

var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');

var port = process.env.PORT || 4000;

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

//load mongoose package
var mongoose = require('mongoose');
var configDB = require('./config/database.js');
//use native node promise
mongoose.Promise = global.Promise;
//connect to mongo db
mongoose.connect(configDB.url)
.then(() => console.log('connection successful'))
.catch((err) => console.log(err));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ 
	secret: 'sshsecret',
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({
		url:configDB.url,
		collection:'sessions'
	})
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var blog_session;

require('./config/passport')(passport);

app.use('/', routes);
app.use('/admin/users', users);
app.use('/admin',admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

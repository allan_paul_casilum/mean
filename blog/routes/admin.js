var express = require('express');
var passport = require('passport');
var router = express.Router();

/* GET home page. */
router.get('/', isLoggedIn, function(req, res, next) {
  res.json({success:true,message:'Successfully Login'});
});

router.get('/login', function(req, res, next) {
	res.render('login', {
		title: 'Login',
		csslink:[
			'signin.css'
		],
		jsfooter:[
			'loginapp.js'
		]
	});
});

router.post('/login', passport.authenticate('local-login', {  
  successRedirect: '/admin',
  failureRedirect: '/',
  failureFlash: true,
}));

router.get('/logout',function(req, res){
	req.logout();
	req.session.destroy();
	res.redirect('/admin/login');
});

module.exports = router;

function isLoggedIn(req, res, next){
	if( req.isAuthenticated() )
		return next();
	res.redirect('/admin/login');	
}

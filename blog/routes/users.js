var express = require('express');
var router = express.Router();

var UserModel = require('../model/User.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('admin users');
});


/* POST Create a user */
router.post('/create', function(req, res, next){
	var _res = {
		success: false,
		data: null,
		msg: 'fail'
	};
	
	var post_email = req.body.email;
	var query_check_if_email_exists = UserModel.where({email:post_email});
	query_check_if_email_exists.findOne(function(err, user){
		if(err){
			_res.data = err;
			res.json(_res);
		}else if(user){
			_res.msg = 'Email Already exists';
			res.json(_res);
		}else{
			var createUser = new UserModel();
			createUser.name = req.body.name;
			createUser.username = req.body.username;
			createUser.email = req.body.email;
			createUser.password = createUser.generateHash(req.body.password);

			createUser.save(function(err){
				if( err ){
					_res.msg = 'error';
					_res.data = err;
				}else{
					_res.msg = 'success';
					_res.data = user;
					_res.body = req.body;
					_res.success = true;
				}
				res.json(_res);
			});
		}
	});
	
});
module.exports = router;

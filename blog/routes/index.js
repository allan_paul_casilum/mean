var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send(req.session);
});

router.get('/seed', function(req, res, next){
	res.json('seed');
});

module.exports = router;

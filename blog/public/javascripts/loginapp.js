//initialize the app
var loginApp = angular.module("loginApp", ['ngRoute']);

//create the route
loginApp.config(['$routeProvider', function($routeProvider){
	
}]);

//create the factory
loginApp.factory('loginFactory', function($http, $q){
	var loginFactory = {};
	
	var _obj_toparams = function(_object){
		var str = "";
		for (var key in _object) {
			if (str != "") {
				str += "&";
			}
			str += key + "=" + encodeURIComponent(_object[key]);
		}
		return str;
	}
	
	loginFactory.Login = function(username, password){
		var deferred = $q.defer();
		
		var login_data = {
			username: username,
			password: password
		};
		$http({
			url:'/admin/login',
			method:'POST',
			data: _obj_toparams(login_data),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(res){
			deferred.resolve(res);
		});
		return deferred.promise;
	};
	
	return loginFactory;
});

//create the service
loginApp.service('loginService', function(loginFactory, $rootScope){
	var _login = loginFactory;
	
	this.login = function(username, password){
		return _login.Login(username, password);
	};
	
});

loginApp.controller('LoginController', function($scope, $routeParams, loginService){
	
	$scope.login = function(){
		var _ret = loginService.login($scope.username, $scope.password);

		_ret.then(function(data){
			//loadList();
			//console.log(data.flash.loginMessage[0]);
			console.log(data);
		});
		/*if( _ret.success ){
			console.log('success');
		}*/
	};
	
});

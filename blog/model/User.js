var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var configDB = require('../config/database.js');

var UserSchema = new mongoose.Schema({
	name: String,
	username: String,
	email: String,
	password: String
},{collection:configDB.mongo_user_collection});

UserSchema.methods.generateHash = function(password){
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password){
	return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
